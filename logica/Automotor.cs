﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace logica
{
    class Automotor : Seguro
    {
        public string Patente { get; set; }
        public string Marca { get; set; }
        public DateTime AñoFabricacion { get; set; }
        public TipoVehiculo Vehiculo { get; set; }
        public int CantidadMaxOcupantes { get; set; }

        public override string DevolverTipo()
        {
            return"automotor";
        }

        public override bool Validacion(int dni)
        {
            if(dni==Beneficiario.DNI)
            {
                if (Vehiculo==0)
                {
                    if (DateTime.Today.Year - AñoFabricacion.Year <= 5)
                    {
                        return true;
                    }
                   
                }
                else
                {
                    return true;
                }
                
            }          
            return false;
        }

        public enum TipoVehiculo { moto, auto, camioneta }  //enumerador


    }
}

