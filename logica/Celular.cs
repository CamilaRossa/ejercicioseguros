﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace logica
{
    public class Celular:Seguro
    {
        public string Modelo { get; set; }
        public int Pulgadas { get; set; }
        public double PrecioActual { get; set; }
        public DateTime FechaCompra { get; set; }

        public override string DevolverTipo()
        {
           return "celular";
        }

        public override bool Validacion(int dni)
        {
           if (dni==Beneficiario.DNI && dni==Tomador.DNI)
            {
                if (Pulgadas<6)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
