﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace logica
{
    
    public class Incendio:Seguro
    {
        public int Metros { get; set; }
        public int CantidadMatafuegos { get; set; }
        public int CantidadEnchufes { get; set; }
        public bool Vivienda { get; set; }
        public double MontoBienes { get; set; }

        public override string DevolverTipo()
        {
            return"incendio";
        }

        public override bool Validacion(int dni)
        {
            if (dni==Beneficiario.DNI) 
            {
                if (CantidadEnchufes<=CantidadMatafuegos)
                {
                    if(Vivienda==true)
                    {
                        if (Metros<400)
                        {
                            return true;
                        }
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            return false;
            
        }
    }
}
