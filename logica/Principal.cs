﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace logica
{
    class Principal
    {
        public List<Seguro> Seguros { get; set; }
        public List<Persona> Personas { get; set; }
        public List<Siniestro> Siniestros { get; set; }

        public List<ReporteSiniestro> Reportes { get; set; }

        public bool RegistrarSiniestro(int dni, DateTime fecha, int numeroPoliza)
        {
            Seguro seguroEncontrado = null;
            seguroEncontrado = Seguros.Find(x => x.NumeroPoliza == numeroPoliza);
          
            if (seguroEncontrado!=null)
            {
                bool esValido=seguroEncontrado.Validacion(dni);
                if (esValido==true)
                {
                    Siniestro nuevoSiniestro = new Siniestro();
                    Persona personaEncontrada = Personas.Find(x => x.DNI == dni);
                    nuevoSiniestro.Cliente = personaEncontrada;
                   
                    nuevoSiniestro.FechaSiniestro = fecha;
                    nuevoSiniestro.SeguroAsociado = seguroEncontrado;
                    Siniestros.Add(nuevoSiniestro);


                    return true;
                }
                return false;
            }
            
            return false;
        }
        public void ListadoReporte(int dni)
        {
            Siniestro siniestroEncontrado = null;
            siniestroEncontrado = Siniestros.Find(x => x.Cliente.DNI == dni);
            if (siniestroEncontrado!=null)
            {
                ReporteSiniestro nuevoReporte = new ReporteSiniestro();
                nuevoReporte.Nombre = siniestroEncontrado.Cliente.Nombre;
                nuevoReporte.FechaSiniestro = siniestroEncontrado.FechaSiniestro;
                nuevoReporte.SeguroAsociado = siniestroEncontrado.SeguroAsociado;
                nuevoReporte.Tipo = siniestroEncontrado.SeguroAsociado.DevolverTipo();
                Reportes.Add(nuevoReporte);
            }
        }
             
    }
}
