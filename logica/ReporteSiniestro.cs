﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace logica
{
    class ReporteSiniestro
    {
        public string Nombre { get; set; }
        public DateTime FechaSiniestro { get; set; }
        public Seguro SeguroAsociado { get; set; }
        public string Tipo { get; set; }

    }
}
