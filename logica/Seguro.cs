﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace logica
{
    public abstract class Seguro
    {
        public Tomador Tomador { get; set; }
        public Beneficiario Beneficiario { get; set; }
        public double Prima { get; set; }
        public int  NumeroPoliza { get; set; }
        public double MontoMensual { get; set; }

        public int PorcentajeSeguro()
        {
            return Convert.ToInt32(MontoMensual * 12 / Prima * 100);
        }
        public abstract bool Validacion(int dni);
        public abstract string DevolverTipo();
       
        
    }
}
