﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace logica
{
    public class Siniestro
    {
        public Persona Cliente { get; set; }
        public DateTime FechaSiniestro { get; set; }
        public Seguro SeguroAsociado { get; set; }


    }
}
